package org.lauro.parkingnantesmetropole.UI.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.lauro.parkingnantesmetropole.Model.Parkings.Parking;
import org.lauro.parkingnantesmetropole.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ParkingAdapter extends ArrayAdapter<Parking> {
    @BindView(R.id.parking_adapter_street)
    TextView mParkingStreetTextView;

    @BindView(R.id.parking_adapter_city)
    TextView mParkingCity;

    @BindView(R.id.parking_adapter_name)
    TextView mParkingName;

    @BindView(R.id.parking_adapter_capacity)
    TextView mParkingCapacity;

    @BindView(R.id.CB_logo)
    ImageView mLogoCB;

    @BindView(R.id.especes_logo)
    ImageView mLogoEspeces;

    @BindView(R.id.logo_total)
    ImageView mLogoTotalGR;

    @BindView(R.id.favorite)
    ImageView mEstFavoris;

    public ParkingAdapter(Context context, List<Parking> parkings) {
        super(context, -1, parkings);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View actualView = convertView;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            actualView = inflater.inflate(R.layout.parking_adapter, parent, false);
        }
        ButterKnife.bind(this, actualView);
        Parking parkingProperties = getItem(position);

        //Valorisation des champs d'information du parking
        mParkingStreetTextView.setText(parkingProperties.adresse);
        mParkingName.setText(parkingProperties.getName());
        mParkingCity.setText(String.valueOf(parkingProperties.getCodePostal())
                + ", " + parkingProperties.getCommune());

        //Valorisation du champ de disponibilité du parking
        if(parkingProperties.getCapaciteParking() != null &&
                parkingProperties.estComplet())
        {
            mParkingCapacity.setText("COMPLET");
        } else {
            mParkingCapacity.setText(
                    String.valueOf(parkingProperties.currentCapa + " / " +
                    String.valueOf(parkingProperties.capaVoitures)));
        }

        //Affichage des icônes de moyen de paiement
        afficherIconesMoyenPaiement(parkingProperties);

        //Affichage de l'icône favoris
        if(parkingProperties.getIsFavorite()) {
            mEstFavoris.setImageResource(R.drawable.plein_heart);
        }else{
            mEstFavoris.setImageResource(R.drawable.blank_heart);
        }

        return actualView;
    }

    /**
     * Affichage des moyens de paiement disponibles pour le parking donné en paramètres
     * @param parking : Parking pour lequel on affiche les moyens de paiement disponibles
     */
    private void afficherIconesMoyenPaiement(Parking parking)
    {
        if(parking.hasCB()) {
            mLogoCB.setVisibility(View.VISIBLE);
        } else {
            mLogoCB.setVisibility(View.INVISIBLE);
        }

        if(parking.hasEspece()) {
            mLogoEspeces.setVisibility(View.VISIBLE);
        } else {
            mLogoEspeces.setVisibility(View.INVISIBLE);
        }

        if(parking.hasTotalGR()) {
            mLogoTotalGR.setVisibility(View.VISIBLE);
        } else {
            mLogoTotalGR.setVisibility(View.INVISIBLE);
        }
    }
}
