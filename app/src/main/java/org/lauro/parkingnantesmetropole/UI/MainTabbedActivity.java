package org.lauro.parkingnantesmetropole.UI;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.FrameLayout;

import org.lauro.parkingnantesmetropole.R;
import org.lauro.parkingnantesmetropole.Service.ParkingFavorisSearchService;
import org.lauro.parkingnantesmetropole.UI.Fragment.ListeFragment;
import org.lauro.parkingnantesmetropole.UI.Fragment.MapsFragment;
import org.lauro.parkingnantesmetropole.Utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainTabbedActivity extends AppCompatActivity {

    Utils u;
    Fragment fragment;

    @BindView(R.id.fragmentContainer)
    FrameLayout frameLayout;

    @BindView(R.id.navigation)
    BottomNavigationView navigation;


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    try {
                        fragment = new ListeFragment();
                        Utils.fragmentManager(frameLayout.getId(), fragment, MainTabbedActivity.this);
                    } catch (Exception e) {
                        e.getStackTrace();
                    }
                    return true;
                case R.id.navigation_dashboard:
                    try {
                        fragment = new MapsFragment();
                        Utils.fragmentManager(frameLayout.getId(), fragment, MainTabbedActivity.this);
                    } catch (Exception e) {
                        e.getStackTrace();
                    }                    return true;
                case R.id.navigation_notifications:
                    try {
                        fragment = new ListeFragment(ParkingFavorisSearchService.INSTANCE);
                        Utils.fragmentManager(frameLayout.getId(), fragment, MainTabbedActivity.this);
                    } catch (Exception e) {
                        e.getStackTrace();
                    }                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_tabbed);
        ButterKnife.bind(this);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        try {
            fragment = new ListeFragment();
            Utils.fragmentManager(frameLayout.getId(), fragment, MainTabbedActivity.this);
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

}
