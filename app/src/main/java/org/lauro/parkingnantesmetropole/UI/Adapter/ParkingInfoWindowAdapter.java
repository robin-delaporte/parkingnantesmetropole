package org.lauro.parkingnantesmetropole.UI.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import org.lauro.parkingnantesmetropole.Model.Parkings.Parking;
import org.lauro.parkingnantesmetropole.R;
import org.lauro.parkingnantesmetropole.UI.ParkingDetailActivity;

import java.util.HashMap;

/**
 * Created by robin_delaporte on 01/05/2018.
 */

//TODO Designer la vue de l'info window

public class ParkingInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {
    private Activity mContext;
    private GoogleMap map;
    private HashMap<Marker, Parking> markers;

    public ParkingInfoWindowAdapter(Activity context, GoogleMap map, HashMap<Marker, Parking> markers){
        this.mContext = context;
        this.map = map;
        this.markers = markers;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker args) {
        View v = mContext.getLayoutInflater().inflate(R.layout.info_window_layout, null);

        TextView title = (TextView) v.findViewById(R.id.title);
        TextView snippet = (TextView) v.findViewById(R.id.body);
        Parking parking = markers.get(args);

        LatLng clickMarkerLatLng = args.getPosition();

        title.setText(args.getTitle());
        snippet.setText(args.getSnippet());

        map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            public void onInfoWindowClick(Marker marker) {
                Intent seeParkingDetailsIntent = new Intent(mContext, ParkingDetailActivity.class);
                Parking parking = markers.get(marker);
                seeParkingDetailsIntent.putExtra("parking", parking);
                mContext.startActivity(seeParkingDetailsIntent);
            }
        });

        return v;
    }
}
