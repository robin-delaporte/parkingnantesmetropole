package org.lauro.parkingnantesmetropole.UI.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Marker;
import com.squareup.otto.Subscribe;

import org.lauro.parkingnantesmetropole.Event.EventBusManager;
import org.lauro.parkingnantesmetropole.Event.SearchResultEvent;
import org.lauro.parkingnantesmetropole.Model.Parkings.Parking;
import org.lauro.parkingnantesmetropole.R;
import org.lauro.parkingnantesmetropole.Service.ParkingSearchService;
import org.lauro.parkingnantesmetropole.UI.Adapter.ParkingInfoWindowAdapter;
import org.lauro.parkingnantesmetropole.Utils.MapHelper;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MapsFragment extends ParkingSearchFragment implements OnMapReadyCallback {

    private GoogleMap mMap;

    @BindView(R.id.activity_main_search_adress_edittext)
    EditText mSearchEditText;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        ButterKnife.bind(this,view);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        super.onCreateView(inflater,container,savedInstanceState);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBusManager.BUS.register(this);
    }

    @Override
    public void onPause() {
        EventBusManager.BUS.unregister(this);

        super.onPause();
    }

    @Subscribe
    public void searchResult(final SearchResultEvent event) {
        mMap.clear();
        HashMap<Marker, Parking> markers = MapHelper.INSTANCE.addMarkersFromParkings(mMap,event.getParkings(),getActivity());
        mMap.setInfoWindowAdapter(new ParkingInfoWindowAdapter(getActivity(), mMap, markers));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        ParkingSearchService.INSTANCE.searchParkings();
    }
}
