package org.lauro.parkingnantesmetropole.UI;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Select;
import com.google.android.gms.maps.OnStreetViewPanoramaReadyCallback;
import com.google.android.gms.maps.StreetViewPanorama;
import com.google.android.gms.maps.SupportStreetViewPanoramaFragment;
import com.google.android.gms.maps.model.LatLng;

import org.lauro.parkingnantesmetropole.Model.Parkings.Parking;
import org.lauro.parkingnantesmetropole.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

//@TODO : Revoir le design de la vue

public class ParkingDetailActivity extends AppCompatActivity {

    @BindView(R.id.nomParking)
    TextView nomParking;

    @BindView(R.id.presentation)
    TextView presentation;

    @BindView(R.id.adresse)
    TextView adresse;

    @BindView(R.id.telephone)
    TextView telephone;

    @BindView(R.id.siteWeb)
    TextView siteWeb;

    @BindView(R.id.capaciteVoiture)
    TextView capaciteVoiture;

    @BindView(R.id.capaciteVoitureElectrique)
    TextView capaciteVoitureElectrique;

    @BindView(R.id.capacitePMR)
    TextView capacitePMR;

    @BindView(R.id.capaciteMoto)
    TextView capaciteMoto;

    @BindView(R.id.capaciteVelo)
    TextView capaciteVelo;

    @BindView(R.id.serviceVelo)
    TextView serviceVelo;

    @BindView(R.id.accesTransportCommun)
    TextView accesTransportCommun;

    @BindView(R.id.conditionsAcces)
    TextView conditionsAcces;

    @BindView(R.id.infosComplementaires)
    TextView informationsComplementaires;

    @BindView(R.id.moyensPaiement)
    TextView moyensPaiement;

    @BindView(R.id.typeParking)
    TextView typeParking;

    @BindView(R.id.exploitant)
    TextView exploitant;

    @BindView(R.id.boutonFavoris)
    ImageButton boutonFavoris;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        ButterKnife.bind(this);

        //Récupération du parking sélectionné par l'utilisateur
        final Parking currentParking = (Parking) getIntent().getSerializableExtra("parking");

        //Affichage des informations
        nomParking.setText(currentParking.getName());
        presentation.setText(currentParking.getPresentation());
        adresse.setText(currentParking.getAdresse()
                + " - " + currentParking.getCodePostal()
                + " , " + currentParking.getCommune());
        telephone.setText("Téléphone : " + currentParking.getTel());
        siteWeb.setText("Site web : " + currentParking.getSiteWeb());

        //Affichage des capacités d'accueil
        capaciteVoiture.setText(String.valueOf(currentParking.getCapaVoitures()));
        capaciteVoitureElectrique.setText(String.valueOf(currentParking.getCapaVehiElec()));
        capacitePMR.setText(String.valueOf(currentParking.getCapaPMR()));
        capaciteMoto.setText(String.valueOf(currentParking.getCapaMoto()));

        String capaciteVeloStr = String.valueOf(currentParking.getCapaVelo());

        if ("OUI".equals(currentParking.getStationnementVeloSecurise())) {
            capaciteVeloStr = capaciteVeloStr + " (sécurisé)";
        }

        capaciteVelo.setText(capaciteVeloStr);
        serviceVelo.setText(currentParking.getServiceVelo());

        //Informations sur l'accès au parking
        if (currentParking.getAccesTransCommuns() != null || currentParking.getCondAcces() != null) {
            if (currentParking.getAccesTransCommuns() != null) {
                accesTransportCommun.setText("Transports en commun : " + currentParking.getAccesTransCommuns());
            }

            if (currentParking.getCondAcces() != null) {
                conditionsAcces.setText("Conditions d'accès : " + currentParking.getCondAcces());
            }
        }

        //Informations complémentaires
        if (currentParking.getInfoComp() != null) {
            informationsComplementaires.setText(currentParking.getInfoComp());
        }

        if (currentParking.getMoyenPaiement() != null) {
            moyensPaiement.setText("Moyens de paiement : " + currentParking.getMoyenPaiement());
        }

        if (currentParking.getLibCategorie() != null && currentParking.getLibType() != null) {
            typeParking.setText("Type de parking : " + currentParking.getLibCategorie()
                    + " - " + currentParking.getLibType());
        }

        if (currentParking.getExploitant() != null) {
            exploitant.setText("Exploitant : " + currentParking.getExploitant());
        }

        //Gestion du bouton Favoris
        if (currentParking.getIsFavorite()) {
            boutonFavoris.setImageResource(R.drawable.plein_heart);
        }

        final View.OnClickListener boutonFavorisHandle = new View.OnClickListener() {
            public void onClick(View v) {
                if (currentParking.getIsFavorite()) {
                    currentParking.setIsFavorite(false);
                    boutonFavoris.setImageResource(R.drawable.blank_heart);
                } else {
                    currentParking.setIsFavorite(true);
                    boutonFavoris.setImageResource(R.drawable.plein_heart);
                }
                currentParking.save();
                List<Parking> loadedParkings = new Select().
                        from(Parking.class)
                        .where("_IDOBJ = " + currentParking.id)
                        .limit(1)
                        .execute();
                Log.i("favori", String.valueOf(loadedParkings.get(0).getIsFavorite()));
            }

        };

        boutonFavoris.setOnClickListener(boutonFavorisHandle);

        //Street View
        SupportStreetViewPanoramaFragment streetViewPanoramaFragment =
                (SupportStreetViewPanoramaFragment)
                        getSupportFragmentManager().findFragmentById(R.id.streetView);
        streetViewPanoramaFragment.getStreetViewPanoramaAsync(
        new OnStreetViewPanoramaReadyCallback() {
            @Override
            public void onStreetViewPanoramaReady(StreetViewPanorama panorama) {
                // Only set the panorama to SYDNEY on startup (when no panoramas have been
                // loaded which is when the savedInstanceState is null).
                if (savedInstanceState == null) {
                    panorama.setPosition(new LatLng(currentParking.getLatitude(),currentParking.getLongitude()));
                }
            }
        });
    }
}
