package org.lauro.parkingnantesmetropole.UI.Fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.SeekBar;

import org.lauro.parkingnantesmetropole.Model.Parkings.Parking;
import org.lauro.parkingnantesmetropole.R;
import org.lauro.parkingnantesmetropole.Service.ParkingSearchService;
import org.lauro.parkingnantesmetropole.UI.Adapter.ParkingAdapter;
import org.lauro.parkingnantesmetropole.UI.ParkingDetailActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnItemClick;

/**
 * Created by robin_delaporte on 04/05/2018.
 */

/**
 * Cette classe permet de mutualiser la recherche des parkings entre les différents controleurs de l'application
 * La doit contenir :
 *  - EditText id : activity_main_search_adress_edittext
 *  - SeekBar id : placeDispoSeekBar
 *  - CheckBox id : coinsButton
 *  - CheckBox id : creditCardButton
 *  - Checkbox id : totalButton
 * La classe enfant doit souscrire au bus d'évenement de la manière suivante :
 *  - Dans le resume : EventBusManager.BUS.unregister(this);
 *  - Définir une méthode de type
 *      @Subscribe
 *      public void searchResult(final SearchResultEvent event) {}
 */
@SuppressLint("ValidFragment")
public class ParkingSearchFragment extends Fragment {

    public ParkingSearchService parkingSearchService = ParkingSearchService.INSTANCE;

    @BindView(R.id.activity_main_search_adress_edittext)
    EditText mSearchEditText;

    @BindView(R.id.placeDispoSeekBar)
    SeekBar mSeekBar;

    @BindView(R.id.coinsButton)
    CheckBox coinsButton;

    @BindView(R.id.creditCardButton)
    CheckBox creditCardButton;

    @BindView(R.id.totalButton)
    CheckBox totalButton;

    public ParkingSearchFragment(ParkingSearchService parkingSearchService) {
        super();
        this.parkingSearchService = parkingSearchService;
    }

    public ParkingSearchFragment(){super();}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = new View(getContext());

        //Gestion de la recherche dynamique
        mSearchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //Lancement de la recherche
                parkingSearchService.searchParkingsByFilters(mSearchEditText.getText().toString(),mSeekBar.getProgress(),coinsButton.isChecked(), creditCardButton.isChecked(),totalButton.isChecked());
            }
        });

        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                parkingSearchService.searchParkingsByFilters(mSearchEditText.getText().toString(),mSeekBar.getProgress(),coinsButton.isChecked(), creditCardButton.isChecked(),totalButton.isChecked());
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @OnCheckedChanged(R.id.coinsButton)
    public void coinsButtonClicked(CompoundButton b, boolean checked) {
        parkingSearchService.searchParkingsByFilters(mSearchEditText.getText().toString(), mSeekBar.getProgress(), coinsButton.isChecked(), creditCardButton.isChecked(), totalButton.isChecked());
    }

    @OnCheckedChanged(R.id.creditCardButton)
    public void creditCardButtonClicked(CompoundButton b, boolean checked) {
        parkingSearchService.searchParkingsByFilters(mSearchEditText.getText().toString(),mSeekBar.getProgress(),coinsButton.isChecked(), creditCardButton.isChecked(),totalButton.isChecked());
    }


    @OnCheckedChanged(R.id.totalButton)
    public void totalButtonClicked(CompoundButton b,boolean checked){
        parkingSearchService.searchParkingsByFilters(mSearchEditText.getText().toString(),mSeekBar.getProgress(),coinsButton.isChecked(), creditCardButton.isChecked(),totalButton.isChecked());
    }
}
