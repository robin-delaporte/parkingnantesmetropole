package org.lauro.parkingnantesmetropole.UI.Fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SeekBar;

import com.squareup.otto.Subscribe;

import org.lauro.parkingnantesmetropole.Event.EventBusManager;
import org.lauro.parkingnantesmetropole.Event.SearchResultEvent;
import org.lauro.parkingnantesmetropole.Model.Parkings.Parking;
import org.lauro.parkingnantesmetropole.R;
import org.lauro.parkingnantesmetropole.Service.ParkingFavorisSearchService;
import org.lauro.parkingnantesmetropole.Service.ParkingSearchService;
import org.lauro.parkingnantesmetropole.UI.Adapter.ParkingAdapter;
import org.lauro.parkingnantesmetropole.UI.ParkingDetailActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.OnItemClick;
import butterknife.Optional;


@SuppressLint("ValidFragment")
public class ListeFragment extends ParkingSearchFragment {

    @Nullable @BindView(R.id.listView)
    ListView mListView;
    private ParkingAdapter mParkingAdapter;

    @BindView(R.id.activity_main_loader)
    ProgressBar mProgressBar;

    public ListeFragment(ParkingFavorisSearchService instance) {
        super(instance);
    }

    public ListeFragment() {
        super();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_liste_parkings, container, false);
        ButterKnife.bind(this, view);

        mParkingAdapter = new ParkingAdapter(getContext(), new ArrayList<Parking>());
        mListView.setAdapter(mParkingAdapter);

        super.onCreateView(inflater,container,savedInstanceState);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBusManager.BUS.register(this);
        this.parkingSearchService.searchParkings();
    }

    @Override
    public void onPause() {
        EventBusManager.BUS.unregister(this);

        super.onPause();
    }

    @OnItemClick(R.id.listView)
    public void onItemSelected(int position) {
        //Lancement de l'activité de vue du parking en détail
        Intent seeParkingDetailsIntent = new Intent(getActivity(), ParkingDetailActivity.class);
        Parking parking = (Parking) mParkingAdapter.getItem(position);
        seeParkingDetailsIntent.putExtra("parking", parking);
        startActivity(seeParkingDetailsIntent);
    }

    @Subscribe
    public void searchResult(final SearchResultEvent event) {
        mParkingAdapter.clear();
        mParkingAdapter.addAll(event.getParkings());
        mProgressBar.setVisibility(View.GONE);

    }
}