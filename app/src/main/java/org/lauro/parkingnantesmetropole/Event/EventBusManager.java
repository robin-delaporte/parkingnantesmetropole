package org.lauro.parkingnantesmetropole.Event;

import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

/**
 * Created by alexmorel on 10/01/2018.
 */

/**
 * Cette classe permet de se souscrire à un bus d'évenement
 *   Exemple:
 *   Dans une activité ou fragment on souscrit au bus :
 *      @Override
 *      public void onResume() {
 *          super.onResume();
 *          EventBusManager.BUS.register(this);
 *      }
 *
 *       @Override
 *       public void onPause() {
 *          EventBusManager.BUS.unregister(this);
 *          super.onPause();
 *       }
 *  L'activité ou le fragment peut alors récupérer les appels fait via le bus
 *  Voir plus en détail : http://square.github.io/otto/
 */
public class EventBusManager {

    public static Bus BUS = new Bus(ThreadEnforcer.ANY);
}
