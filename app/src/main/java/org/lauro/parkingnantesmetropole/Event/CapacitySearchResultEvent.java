package org.lauro.parkingnantesmetropole.Event;


import org.lauro.parkingnantesmetropole.Model.Capacities.Capacity;

import java.util.List;

/**
 * Cette classe permet définir l'évenement de récupération de la capacité des parkings
 * Exemple :
 *      Après avoir souscrit au bus d'évenement, on souscrit une méthode à cet évennement spécifique :
 *      @Subscribe
 *       public void searchResult(final CapacitySearchResultEvent event) {
 *           List<Capacity> capacities = event.getCapacities();
 *       }
 */
public class CapacitySearchResultEvent {

    private List<Capacity> capacityList;

    public CapacitySearchResultEvent(List<Capacity> places) {
        this.capacityList = places;
    }

    public List<Capacity> getCapacities() {
        return capacityList;
    }
}
