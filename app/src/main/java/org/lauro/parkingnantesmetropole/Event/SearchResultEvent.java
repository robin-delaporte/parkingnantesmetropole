package org.lauro.parkingnantesmetropole.Event;


import org.lauro.parkingnantesmetropole.Model.Parkings.Parking;

import java.util.List;

/**
 * Created by alexmorel on 10/01/2018.
 */

/**
 * Cette classe permet définir l'évenement de récupération des parkings
 * Exemple :
 *      Après avoir souscrit au bus d'évenement, on souscrit une méthode à cet évennement spécifique :
 *      @Subscribe
 *       public void searchResult(final SearchResultEvent event) {
 *           List<Parking> parking = event.getParkings();
 *       }
 */
public class SearchResultEvent {

    private List<Parking> parkingList;

    public SearchResultEvent(List<Parking> places) {
        this.parkingList = places;
    }

    public List<Parking> getParkings() {
        return parkingList;
    }
}
