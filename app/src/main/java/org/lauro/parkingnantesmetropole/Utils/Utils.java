package org.lauro.parkingnantesmetropole.Utils;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;



/**
 * Created by robin_delaporte on 20/04/2018.
 */

public class Utils {
    /**
     * Permet de switcher entre les différents fragments de la vue
     * @param contentId
     * @param fragment
     * @param activity
     */
    public static void fragmentManager(Integer contentId, Fragment fragment, AppCompatActivity activity) {
        try {
            FragmentManager fm = activity.getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.replace(contentId, fragment);
            fragmentTransaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
