package org.lauro.parkingnantesmetropole.Utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.lauro.parkingnantesmetropole.Model.Parkings.Parking;
import org.lauro.parkingnantesmetropole.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by robin_delaporte on 01/05/2018.
 */

public class MapHelper {
    public static final MapHelper INSTANCE = new MapHelper();

    /**
     * Cette méthode permet d'ajouter des Markers google map sur une map depuis une liste de Parking
     * @param mMap
     * @param parkings
     * @param context
     * @return HashMap<Marker, Parking>
     */
    public HashMap<Marker, Parking> addMarkersFromParkings(GoogleMap mMap, List<Parking> parkings, Context context) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        HashMap<Marker, Parking> markers = new HashMap<>();

        for (Parking parking : parkings) {
            Bitmap imageBitmap = BitMapHelper.getBitmap(context, R.drawable.ic_map_pin_parking);
            Bitmap resizedBitmap = Bitmap.createScaledBitmap(imageBitmap, 50, 50, false);

            MarkerOptions markerOption = new MarkerOptions()
                    .position(new LatLng(parking.getLatitude(), parking.getLongitude()))
                    .title(parking.name)
                    .snippet(parking.getCodePostal() + "  " + parking.getAdresse() + ".\n Places disponibles : " + parking.getCapaVoitures())
                    .icon(BitmapDescriptorFactory.fromBitmap(resizedBitmap));

            Marker m = mMap.addMarker(markerOption);
            builder.include(markerOption.getPosition());
            markers.put(m,parking);
        }

        int width = context.getResources().getDisplayMetrics().widthPixels;
        int height = context.getResources().getDisplayMetrics().heightPixels;
        int padding = (int) (width * 0.10); // offset from edges of the map 10% of screen

        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(builder.build(), width, height, padding);

        mMap.animateCamera(cu);

        return markers;

    }
}
