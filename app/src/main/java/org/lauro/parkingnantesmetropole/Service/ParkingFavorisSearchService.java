package org.lauro.parkingnantesmetropole.Service;

import com.activeandroid.query.Select;

import org.lauro.parkingnantesmetropole.Event.EventBusManager;
import org.lauro.parkingnantesmetropole.Event.SearchResultEvent;
import org.lauro.parkingnantesmetropole.Model.Parkings.Parking;

import java.util.List;

/**
 * Created by robin_delaporte on 01/05/2018.
 */

/**
 * Service spécifique aux parking en favoris
 */

public class ParkingFavorisSearchService extends ParkingSearchService {
    public static ParkingFavorisSearchService INSTANCE = new ParkingFavorisSearchService();
    /**
     * Constructeur du service
     */
    private ParkingFavorisSearchService() {
        super();
    }

    @Override
    public void searchParkings() {
        // Get places matching the search from DB
        List<Parking> matchingPlacesFromDB = new Select().
                from(Parking.class)
                .where("favorite = 1")
                .orderBy("ADRESSE")
                .execute();
        // Post result as an event
        EventBusManager.BUS.post(new SearchResultEvent(matchingPlacesFromDB));
    }

    /**
     * Cette méthode permet de filtrer les parkings mis en FAVORIS en fonction des paramètres en entrée
     * La méthode fait ensuite appel au bus d'évenement SearchResultEvent pour l'envoie des résultats
     * @param name
     * @param nbPlaceDispo
     * @param coin
     * @param cb
     * @param total
     */
    @Override
    public void searchParkingsByFilters(String name, int nbPlaceDispo, Boolean coin, Boolean cb, Boolean total){
        String whereStatement = "";
        if (name != null){
            whereStatement += "(ADRESSE like '%"+name.toLowerCase()+"%'";
            whereStatement += " or NAME like '%"+name.toLowerCase()+"%'";
            whereStatement += " or COMMUNE like '%"+name.toLowerCase()+"%')";
        }
        whereStatement += " and currentCapa >= "+nbPlaceDispo;
        if (coin)
            whereStatement += " and MOYEN_PAIEMENT like '%Esp%'";
        if (cb)
            whereStatement += " and MOYEN_PAIEMENT like '%CB%'";
        if (total)
            whereStatement += " and MOYEN_PAIEMENT like '%Total GR%'";
        List<Parking> matchingPlacesFromDB = new Select().
                from(Parking.class)
                .where(whereStatement)
                .and("favorite = 1")
                .orderBy("ADRESSE")
                .execute();
        // Post result as an event
        EventBusManager.BUS.post(new SearchResultEvent(matchingPlacesFromDB));
    }
}
