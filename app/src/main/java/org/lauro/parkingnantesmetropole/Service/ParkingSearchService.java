package org.lauro.parkingnantesmetropole.Service;


import android.view.View;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Select;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.otto.Subscribe;

import org.lauro.parkingnantesmetropole.Event.*;

import org.lauro.parkingnantesmetropole.Event.CapacitySearchResultEvent;
import org.lauro.parkingnantesmetropole.Model.Capacities.Capacity;
import org.lauro.parkingnantesmetropole.Model.Capacities.Response;
import org.lauro.parkingnantesmetropole.Model.Parkings.Parking;
import org.lauro.parkingnantesmetropole.Model.Parkings.Parkings;
import org.lauro.parkingnantesmetropole.Event.EventBusManager;
import org.lauro.parkingnantesmetropole.Event.SearchResultEvent;

import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.List;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;


/**
 * Ce service permet de gérer l'appel et la récupération des parking de la ville de Nantes.
 * Pour fonctionner il faut se documenter concernant le bus d'évenement Otto
 * Pour appeler une des méthodes, il est mieux d'utiliser la variable statique prévue à cet effet
 */

//TODO Revoir l'appel à l'API pour qu'il se fasse toute les x minutes et non à chaque rafraichissement de la vue
public class ParkingSearchService {

    public static ParkingSearchService INSTANCE = new ParkingSearchService();
    private final ParkingSearchRESTService mParkingSearchRESTService;
    private final CapacitySearchRESTService mCapacitySearchRESTService;

    /**
     * Constructeur du service
     */
    public ParkingSearchService() {
        Gson gsonConverter = new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                .serializeNulls()
                .excludeFieldsWithoutExposeAnnotation().create();

        Retrofit retrofit = new Retrofit.Builder()
                .client(new OkHttpClient())
                .baseUrl("https://data.nantes.fr/api/")
                .addConverterFactory(GsonConverterFactory.create(gsonConverter))
                .build();

        EventBusManager.BUS.register(this);

        mParkingSearchRESTService = retrofit.create(ParkingSearchRESTService.class);

        mCapacitySearchRESTService = retrofit.create(CapacitySearchRESTService.class);
    }

    /**
     * Méthode de recherche de parking
     * Cette méthode appel l'API mise à disposition et stocke les résultats dans la base de données
     * Elle va ensuite appelé la méthode de mise à jour des capacités
     * In fine il faut souscrire au bus d'évenement SearchResultEvent
     */
    public void searchParkings() {
        mParkingSearchRESTService.searchForParkings().enqueue(new Callback<Parkings>() {
            @Override
            public void onResponse(Call<Parkings> call, retrofit2.Response<Parkings> response) {

                // Post d'un événement via le bus pour les activités à l'écoute
                if (response.body() != null && response.body().parkingList != null) {
                    HashMap<Integer,Parking> listParking = new HashMap<>();
                    for (Parking parking : response.body().parkingList) {
                        parking.setDefault();
                        List<Parking> loadedParkings = new Select().
                                from(Parking.class)
                                .where("_IDOBJ = " + parking.id)
                                .limit(1)
                                .execute();
                        if (loadedParkings.size() > 0){
                            listParking.put(parking.id, loadedParkings.get(0));
                        }else {
                            listParking.put(parking.id, parking);
                        }
                    }

                    searchCapacity(listParking);

                } else if(response.body() == null ){
                    Log.e("searchParkings", "L'API n'a renvoyé aucun résultat");
                } else {
                    Log.e("searchParkings", "Code erreur : "+response.code()
                            + "Message erreur : " + response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<Parkings> call, Throwable t) {
                t.printStackTrace();
            }
        });

        searchParkingFromDB();
    }

    /**
     * Cette méthode permet de récupérer la liste des capacités des parkings en temps réél
     * On fait appel à l'API mise à disposition
     * Pour récupérer les résultat il faut souscrire au bus d'évenements CapacitySearchResultEvent
     * @param listParking
     */
    public void searchCapacity(final HashMap<Integer,Parking> listParking){
        mCapacitySearchRESTService.searchForCapacity().enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                if (response.body() != null && response.body().openData != null) {
                    ActiveAndroid.beginTransaction();
                    Parking parking;
                    for (Capacity capacity : response.body().openData.answer.data.grpParkings.capacities) {
                        parking = listParking.get(capacity.idFromRest);
                        if (parking != null){
                            parking.updateCapacity(capacity.currentCapa,capacity.maxCapa);
                        }
                    }
                    ActiveAndroid.setTransactionSuccessful();
                    ActiveAndroid.endTransaction();

                    EventBusManager.BUS.post(new CapacitySearchResultEvent(response.body().openData.answer.data.grpParkings.capacities));
                } else if(response.body() == null ){
                } else {

                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                Log.e("capacity response", t.toString());
            }
        });
    }

    @Subscribe
    public void searchCapacityResult(final CapacitySearchResultEvent event) {

        searchParkingFromDB();

    }

    /**
     * Cette classe permet de récupérer l'ensemble des parkings stockés en local sur base de données
     * La méthode fait ensuite appel au bus d'évenement SearchResultEvent pour l'envoie des résultats
     */
    private void searchParkingFromDB() {
        // Get places matching the search from DB
        List<Parking> matchingPlacesFromDB = new Select().
                from(Parking.class)
                .orderBy("ADRESSE")
                .execute();
        // Post result as an event
        EventBusManager.BUS.post(new SearchResultEvent(matchingPlacesFromDB));
    }

    /**
     * Cette méthode permet de filtrer les parkings en fonction des paramètres en entrée
     * La méthode fait ensuite appel au bus d'évenement SearchResultEvent pour l'envoie des résultats
     * @param name
     * @param nbPlaceDispo
     * @param coin
     * @param cb
     * @param total
     */
    public void searchParkingsByFilters(String name, int nbPlaceDispo, Boolean coin, Boolean cb, Boolean total){
        String whereStatement = "";
        if (name != null){
            whereStatement += "(ADRESSE like '%"+name.toLowerCase()+"%'";
            whereStatement += " or NAME like '%"+name.toLowerCase()+"%'";
            whereStatement += " or COMMUNE like '%"+name.toLowerCase()+"%')";
        }
        whereStatement += " and currentCapa >= "+nbPlaceDispo;
        if (coin)
            whereStatement += " and MOYEN_PAIEMENT like '%Esp%'";
        if (cb)
            whereStatement += " and MOYEN_PAIEMENT like '%CB%'";
        if (total)
            whereStatement += " and MOYEN_PAIEMENT like '%Total GR%'";
        List<Parking> matchingPlacesFromDB = new Select().
                from(Parking.class)
                .where(whereStatement)
                .orderBy("ADRESSE")
                .execute();
        // Post result as an event
        EventBusManager.BUS.post(new SearchResultEvent(matchingPlacesFromDB));
    }

    public interface CapacitySearchRESTService {

        @GET("getDisponibiliteParkingsPublics/1.0/U4FD2BMF4MJDXXR/?output=json")
        Call<Response> searchForCapacity();
    }

    /**
     * Service REST pour la recherche de parking
     */
    public interface ParkingSearchRESTService {

        @GET("publication/24440040400129_NM_NM_00044/LISTE_SERVICES_PKGS_PUB_NM_STBL/content/?format=json/")
        Call<Parkings> searchForParkings();
    }
}
