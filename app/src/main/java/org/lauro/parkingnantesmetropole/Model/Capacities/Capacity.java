package org.lauro.parkingnantesmetropole.Model.Capacities;

import com.activeandroid.Model;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by robin_delaporte on 16/04/2018.
 */
@Table(name = "Capacity")
public class Capacity extends Model {


    public Capacity(int currentCapa, int idFromRest, int maxCapa, int nbPlacesMinComplet) {
        this.currentCapa = currentCapa;
        this.idFromRest = idFromRest;
        this.maxCapa = maxCapa;
        this.nbPlacesMinComplet = nbPlacesMinComplet;
    }

    @SerializedName("Grp_disponible")
    @Expose
    public int currentCapa;

    @SerializedName("IdObj")
    @Expose
    public int idFromRest;

    @SerializedName("Grp_exploitation")
    @Expose
    public int maxCapa;

    @SerializedName("Grp_complet")
    @Expose
    public int nbPlacesMinComplet;

    public int getCurrentCapa() {
        return currentCapa;
    }

    public void setCurrentCapa(int currentCapa) {
        this.currentCapa = currentCapa;
    }

    public int getMaxCapa() {
        return maxCapa;
    }

    public void setMaxCapa(int maxCapa) {
        this.maxCapa = maxCapa;
    }

    public int getNbPlacesMinComplet() {
        return nbPlacesMinComplet;
    }

    public void setNbPlacesMinComplet(int nbPlacesMinComplet) {
        this.nbPlacesMinComplet = nbPlacesMinComplet;
    }

    /**
     * Détermine si un parking est complet
     * @return true si le parking est complet
     */
    public boolean estComplet() {
        return(this.getCurrentCapa() <= this.getNbPlacesMinComplet());
    }
}
