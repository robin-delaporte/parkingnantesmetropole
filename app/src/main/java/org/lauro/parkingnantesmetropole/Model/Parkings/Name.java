package org.lauro.parkingnantesmetropole.Model.Parkings;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by robin_delaporte on 09/02/2018.
 */

public class Name implements Serializable {
    @SerializedName("name")
    @Expose
    public String name = "";
}

