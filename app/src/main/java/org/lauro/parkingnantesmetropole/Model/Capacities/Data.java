package org.lauro.parkingnantesmetropole.Model.Capacities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by robin_delaporte on 16/04/2018.
 */

public class Data {
    @SerializedName("Groupes_Parking")
    @Expose
    public GrpParkings grpParkings;
}
