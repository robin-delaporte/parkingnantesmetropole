package org.lauro.parkingnantesmetropole.Model.Parkings;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by robin_delaporte on 09/02/2018.
 */

public class Parkings {
    @SerializedName("data")
    @Expose
    public List<Parking> parkingList;
}
