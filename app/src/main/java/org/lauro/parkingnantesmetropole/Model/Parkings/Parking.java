package org.lauro.parkingnantesmetropole.Model.Parkings;

import android.util.Log;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.lauro.parkingnantesmetropole.Model.Capacities.Capacity;

import java.io.Serializable;

/**
 * Created by robin_delaporte on 09/02/2018.
 */
//TODO Revoir le model pour qu'il soit plus propre.

@Table(name = "Parking")
public class Parking extends Model  implements Serializable{

    private Capacity capaciteParking;


    public Parking(){
        super();
    }

    public Parking(Name geo, double[] latLong, String serviceVelo, int capaVoitures, String infoComp, String stationnementVelo, String stationnementVeloSecurise, int capaVehiElec, int codePostal, String accesPMR, String tel, String moyenPaiement, String accesTransCommuns, int capaMoto, String condAcces, String commune, int capaVelo, String presentation, int capaPMR, String libCategorie, String exploitant, String siteWeb, String adresse, String libType, int id, int currentCapa, int maxCapa) {
        super();
        this.geo = geo;
        this.latLong = latLong;
        this.serviceVelo = serviceVelo;
        this.capaVoitures = capaVoitures;
        this.infoComp = infoComp;
        this.stationnementVelo = stationnementVelo;
        this.stationnementVeloSecurise = stationnementVeloSecurise;
        this.capaVehiElec = capaVehiElec;
        this.codePostal = codePostal;
        this.accesPMR = accesPMR;
        this.tel = tel;
        this.moyenPaiement = moyenPaiement;
        this.accesTransCommuns = accesTransCommuns;
        this.capaMoto = capaMoto;
        this.condAcces = condAcces;
        this.commune = commune;
        this.capaVelo = capaVelo;
        this.presentation = presentation;
        this.capaPMR = capaPMR;
        this.libCategorie = libCategorie;
        this.exploitant = exploitant;
        this.siteWeb = siteWeb;
        this.adresse = adresse;
        this.libType = libType;
        this.id = id;
        this.currentCapa = currentCapa;
        this.maxCapa = maxCapa;
    }

    @SerializedName("geo")
    @Expose
    public Name geo;

    @Column(name="name")
    public String name;

    @SerializedName("_l")
    @Expose
    public double[] latLong;

    @Column(name="latitude")
    private double latitude;

    @Column(name="longitude")
    private double longitude;

    @SerializedName("SERVICE_VELO")
    @Column(name="SERVICE_VELO")
    @Expose
    public String serviceVelo;

    @SerializedName("CAPACITE_VOITURE")
    @Column(name="CAPACITE_VOITURE")
    @Expose
    public int capaVoitures;

    @SerializedName("INFOS_COMPLEMENTAIRES")
    @Column(name="INFOS_COMPLEMENTAIRES")
    @Expose
    public String infoComp;

    @SerializedName("STATIONNEMENT_VELO")
    @Column(name="STATIONNEMENT_VELO")
    @Expose
    public String stationnementVelo;

    @SerializedName("STATIONNEMENT_VELO_SECURISE")
    @Column(name="STATIONNEMENT_VELO_SECURISE")
    @Expose
    public String stationnementVeloSecurise;

    @SerializedName("CAPACITE_VEHICULE_ELECTRIQUE")
    @Column(name="CAPACITE_VEHICULE_ELECTRIQUE")
    @Expose
    public int capaVehiElec;

    @SerializedName("CODE_POSTAL")
    @Column(name="CODE_POSTAL")
    @Expose
    public int codePostal;

    @SerializedName("ACCES_PMR")
    @Column(name="ACCES_PMR")
    @Expose
    public String accesPMR;

    @SerializedName("TELEPHONE")
    @Column(name="TELEPHONE")
    @Expose
    public String tel;

    @SerializedName("MOYEN_PAIEMENT")
    @Column(name="MOYEN_PAIEMENT")
    @Expose
    public String moyenPaiement;

    @SerializedName("ACCES_TRANSPORTS_COMMUNS")
    @Column(name="ACCES_TRANSPORTS_COMMUNS")
    @Expose
    public String accesTransCommuns;

    @SerializedName("CAPACITE_MOTO")
    @Column(name="CAPACITE_MOTO")
    @Expose
    public int capaMoto;

    @SerializedName("CONDITIONS_D_ACCES")
    @Column(name="CONDITIONS_D_ACCES")
    @Expose
    public String condAcces;

    @SerializedName("COMMUNE")
    @Column(name="COMMUNE")
    @Expose
    public String commune;

    @SerializedName("CAPACITE_VELO")
    @Column(name="CAPACITE_VELO")
    @Expose
    public int capaVelo;

    @SerializedName("PRESENTATION")
    @Column(name="PRESENTATION")
    @Expose
    public String presentation;

    @SerializedName("CAPACITE_PMR")
    @Column(name="CAPACITE_PMR")
    @Expose
    public int capaPMR;

    @SerializedName("LIBCATEGORIE")
    @Column(name="LIBCATEGORIE")
    @Expose
    public String libCategorie;

    @SerializedName("EXPLOITANT")
    @Column(name="EXPLOITANT")
    @Expose
    public String exploitant;

    @SerializedName("SITE_WEB")
    @Column(name="SITE_WEB")
    @Expose
    public String siteWeb;

    @SerializedName("ADRESSE")
    @Column(name="ADRESSE")
    @Expose
    public String adresse;

    @SerializedName("LIBTYPE")
    @Column(name="LIBTYPE")
    @Expose
    public String libType;

    @SerializedName("_IDOBJ")
    @Column(name="_IDOBJ", index = true, unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    @Expose
    public int id;

    @Column(name="favorite")
    private boolean isFavorite;

    @Column(name="currentCapa")
    public int currentCapa;

    @Column(name="maxCapa")
    public int maxCapa;

    public void setDefault() {
        if (name == null)
            name = "";
        if (serviceVelo == null)
            serviceVelo = "";
        if (infoComp == null)
            infoComp = "";
        if (stationnementVelo == null)
            stationnementVelo = "";
        if (stationnementVeloSecurise == null)
            stationnementVeloSecurise = "";
        if (accesPMR == null)
            accesPMR = "";
        if (tel == null)
            tel = "";
        if (moyenPaiement == null)
            moyenPaiement = "";
        if (accesTransCommuns == null)
            accesTransCommuns = "";
        if (condAcces == null)
            condAcces = "";
        if (commune == null)
            commune = "";
        if (presentation == null)
            presentation = "";
        if (libCategorie == null)
            libCategorie = "";
        if (exploitant == null)
            exploitant = "";
        if (siteWeb == null)
            siteWeb = "";
        if (adresse == null)
            adresse = "";
        if (libType == null)
            libType = "";
        if (geo != null)
            name = geo.name;
        else{
            name = "Non trouvé";
        }
        if (latLong != null && this.latLong.length == 2){
            latitude = latLong[0];
            longitude = latLong[1];
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double[] getLatLong() {
        return latLong;
    }

    public void setLatLong(double[] latLong) {
        this.latLong = latLong;
    }

    public String getServiceVelo() {
        return serviceVelo;
    }

    public void setServiceVelo(String serviceVelo) {
        this.serviceVelo = serviceVelo;
    }

    public int getCapaVoitures() {
        return capaVoitures;
    }

    public void setCapaVoitures(int capaVoitures) {
        this.capaVoitures = capaVoitures;
    }

    public String getInfoComp() {
        return infoComp;
    }

    public void setInfoComp(String infoComp) {
        this.infoComp = infoComp;
    }

    public String getStationnementVelo() {
        return stationnementVelo;
    }

    public void setStationnementVelo(String stationnementVelo) {
        this.stationnementVelo = stationnementVelo;
    }

    public String getStationnementVeloSecurise() {
        return stationnementVeloSecurise;
    }

    public void setStationnementVeloSecurise(String stationnementVeloSecurise) {
        this.stationnementVeloSecurise = stationnementVeloSecurise;
    }

    public int getCapaVehiElec() {
        return capaVehiElec;
    }

    public void setCapaVehiElec(int capaVehiElec) {
        this.capaVehiElec = capaVehiElec;
    }

    public int getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(int codePostal) {
        this.codePostal = codePostal;
    }

    public String getAccesPMR() {
        return accesPMR;
    }

    public void setAccesPMR(String accesPMR) {
        this.accesPMR = accesPMR;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getMoyenPaiement() {
        return moyenPaiement;
    }

    public void setMoyenPaiement(String moyenPaiement) {
        this.moyenPaiement = moyenPaiement;
    }

    public String getAccesTransCommuns() {
        return accesTransCommuns;
    }

    public void setAccesTransCommuns(String accesTransCommuns) {
        this.accesTransCommuns = accesTransCommuns;
    }

    public int getCapaMoto() {
        return capaMoto;
    }

    public void setCapaMoto(int capaMoto) {
        this.capaMoto = capaMoto;
    }

    public String getCondAcces() {
        return condAcces;
    }

    public void setCondAcces(String condAcces) {
        this.condAcces = condAcces;
    }

    public String getCommune() {
        return commune;
    }

    public void setCommune(String commune) {
        this.commune = commune;
    }

    public int getCapaVelo() {
        return capaVelo;
    }

    public void setCapaVelo(int capaVelo) {
        this.capaVelo = capaVelo;
    }

    public String getPresentation() {
        return presentation;
    }

    public void setPresentation(String presentation) {
        this.presentation = presentation;
    }

    public int getCapaPMR() {
        return capaPMR;
    }

    public void setCapaPMR(int capaPMR) {
        this.capaPMR = capaPMR;
    }

    public String getLibCategorie() {
        return libCategorie;
    }

    public void setLibCategorie(String libCategorie) {
        this.libCategorie = libCategorie;
    }

    public String getExploitant() {
        return exploitant;
    }

    public void setExploitant(String exploitant) {
        this.exploitant = exploitant;
    }

    public String getSiteWeb() {
        return siteWeb;
    }

    public void setSiteWeb(String siteWeb) {
        this.siteWeb = siteWeb;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getLibType() {
        return libType;
    }

    public void setLibType(String libType) {
        this.libType = libType;
    }

    public Capacity getCapaciteParking() {
        return this.capaciteParking;
    }

    public void setCapaciteParking(Capacity capaciteParking) {
        this.capaciteParking = capaciteParking;
    }

    public boolean getIsFavorite() {
        return this.isFavorite;
    }

    public void setIsFavorite(boolean favorite) {
        this.isFavorite = favorite;
    }

    public void updateCapacity(int currentCapa, int maxCapa){
        try {
            this.currentCapa = currentCapa;
            this.maxCapa = maxCapa;
            this.save();
        }catch(Exception e){
            Log.e("Exception", "Erreur lors de la mise à jour de la capacité");
        }
    }

    /**
     * Détermine si un parking est complet
     * @return true si le parking est complet
     */
    public boolean estComplet() {
        return(this.currentCapa <= this.capaVoitures);
    }

    public double getLatitude(){
        return latitude;
    }

    public double getLongitude(){
        return longitude;
    }

    public Boolean hasCB(){
        return moyenPaiement.contains("CB en borne de sortie");
    }

    public Boolean hasEspece(){
        return moyenPaiement.contains("Espèces");
    }

    public Boolean hasTotalGR(){
        return moyenPaiement.contains("Total GR");
    }
}
